use std::ops;

pub trait Counter {
    fn increase(&mut self, initial: usize, rollover: usize);
    fn to_word<T>(&self, alphabet: &T) -> String where T: ops::Index<usize, Output = char>;
}

impl Counter for Vec<usize> {
    fn increase(&mut self, leading_initial: usize, rollover: usize) {
        if leading_initial >= rollover {
            panic!("Invalid rollover, must be > leading_initial");
        }

        for elem in self.iter_mut() {
            *elem += 1;
            if *elem == rollover {
                *elem = 0;
            } else {
                return
            }
        }
        self.push(leading_initial)
    }

    fn to_word<T>(&self, alphabet: &T) -> String
            where T: ops::Index<usize, Output = char> {
        let mut result = String::with_capacity(self.len());
        for &elem in self.iter().rev() {
            result.push(alphabet[elem]);
        }
        result
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_increase() {
        let mut v: Vec<usize> = vec![1, 2, 3];
        v.increase(0, 4);
        assert_eq!(v, vec![2, 2, 3]);
        v.increase(0, 4);
        assert_eq!(v, vec![3, 2, 3]);
        v.increase(0, 4);
        assert_eq!(v, vec![0, 3, 3]);
        v.increase(0, 4);
        assert_eq!(v, vec![1, 3, 3]);
        v.increase(0, 4);
        assert_eq!(v, vec![2, 3, 3]);
        v.increase(0, 4);
        assert_eq!(v, vec![3, 3, 3]);
        v.increase(0, 4);
        assert_eq!(v, vec![0, 0, 0, 0]);
    }

    #[test]
    fn test_increase_empty() {
        let mut v: Vec<usize> = Vec::new();
        v.increase(0, 2);
        assert_eq!(v, vec![0]);
        v.increase(0, 2);
        assert_eq!(v, vec![1]);
        v.increase(0, 2);
        assert_eq!(v, vec![0, 0]);
        v.increase(0, 2);
        assert_eq!(v, vec![1, 0]);
    }

    #[test]
    #[should_panic]
    fn test_increase_rollover_0_panic() {
        let mut v: Vec<usize> = Vec::new();
        v.increase(0, 0);
    }

    #[test]
    #[should_panic]
    fn test_increase_initial_ge_rollover_panic() {
        let mut v: Vec<usize> = Vec::new();
        v.increase(3, 3);
    }

    #[test]
    fn test_increase_rollover_1() {
        let mut v: Vec<usize> = Vec::new();
        v.increase(0, 1);
        assert_eq!(v, vec![0]);
        v.increase(0, 1);
        assert_eq!(v, vec![0, 0]);
        v.increase(0, 1);
        assert_eq!(v, vec![0, 0, 0]);
    }

    #[test]
    fn test_increase_rollover_large() {
        let mut v: Vec<usize> = Vec::new();
        for i in 0..1000 {
            v.increase(0, 9999);
            assert_eq!(v.len(), 1);
            assert_eq!(v[0], i);
        }
    }

    #[test]
    fn test_to_empty_word() {
        let v: Vec<usize> = Vec::new();
        assert_eq!("", v.to_word(&vec!['a']));
    }

    #[test]
    fn test_to_word() {
        let alpha: Vec<char> = vec!['a', 'b', 'c'];
        let mut v: Vec<usize> = Vec::new();

        v.increase(0, alpha.len());
        assert_eq!("a", v.to_word(&alpha));

        v.increase(0, alpha.len());
        assert_eq!("b", v.to_word(&alpha));

        v.increase(0, alpha.len());
        assert_eq!("c", v.to_word(&alpha));

        v.increase(0, alpha.len());
        assert_eq!("aa", v.to_word(&alpha));

        v.increase(0, alpha.len());
        assert_eq!("ab", v.to_word(&alpha));

        v.increase(0, alpha.len());
        assert_eq!("ac", v.to_word(&alpha));

        v.increase(0, alpha.len());
        assert_eq!("ba", v.to_word(&alpha));
    }

    #[test]
    fn test_to_word_uses_reverse() {
        let alpha: Vec<char> = vec!['a', 'b', 'c'];
        let v: Vec<usize> = vec![2, 1, 0];
        assert_eq!("abc", v.to_word(&alpha));
    }
}