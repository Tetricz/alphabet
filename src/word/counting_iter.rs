use crate::counter::Counter;
use crate::util::VectorAccess;

pub struct CountingIterator<'a> {
    source: VectorAccess<'a, char>,
    counter: Vec<usize>
}

impl<'a> CountingIterator<'a> {
    pub fn new(source: VectorAccess<'a, char>) -> Self {
        Self {
            source,
            counter: Vec::new()
        }
    }
}

impl Iterator for CountingIterator<'_> {
    type Item = String;

    fn next(&mut self) -> Option<String> {
        if self.source.get().len() <= 1 {
            None
        } else {
            if self.counter.len() == 0 {
                // Increase to reach zero-value; note that initial is not 1, as this is the only
                // time a leading zero is allowed.
                Counter::increase(&mut self.counter, 0, self.source.get().len());
            }

            let result = self.counter.to_word(self.source.get());
            Counter::increase(&mut self.counter, 1, self.source.get().len());
            Some(result)
        }
    }
}

#[cfg(test)]
mod tests {
    use more_asserts::*;
    use super::*;

    fn create_access<'a>() -> VectorAccess<'a, char> {
        VectorAccess::Direct(vec!['a', 'b', 'c'])
    }

    fn create_empty_access<'a>() -> VectorAccess<'a, char> {
        VectorAccess::Direct(Vec::new())
    }

    fn create_singleton_access<'a>() -> VectorAccess<'a, char> {
        VectorAccess::Direct(vec!['a'])
    }

    #[test]
    fn test_create_iterator() {
        let access = create_access();
        let _iter = CountingIterator::new(access);
    }

    #[test]
    fn test_not_obtain_empty_word() {
        let access = create_access();
        let mut iter = CountingIterator::new(access);
        assert_ne!("", iter.next().unwrap());
    }

    #[test]
    fn test_empty_on_empty_source() {
        let access = create_empty_access();
        let mut iter = CountingIterator::new(access);
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_empty_on_singleton_source() {
        let access = create_singleton_access();
        let mut iter = CountingIterator::new(access);
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_stays_none_on_empty_source() {
        let access = create_empty_access();
        let mut iter = CountingIterator::new(access);
        iter.next();
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_stays_none_on_singleton_source() {
        let access = create_singleton_access();
        let mut iter = CountingIterator::new(access);
        iter.next();
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
        assert!(iter.next().is_none());
    }

    #[test]
    fn test_does_not_end() {
        let access = create_access();
        let mut iter = CountingIterator::new(access);

        // Infinity is hard to reach... this will have to be enough
        for _ in 0..10000 {
            assert!(!iter.next().is_none());
        }
    }

    #[test]
    fn test_word_size_monotonically_increases() {
        let access = create_access();
        let mut iter = CountingIterator::new(access);

        // Infinity is hard to reach... this will have to be enough
        let mut last_len = 1;
        for _ in 0..10000 {
            let next = iter.next().unwrap();
            assert_ge!(next.len(), last_len);
            last_len = next.len();
        }
    }

    #[test]
    fn test_abcba_is_not_a_word() {
        let access = create_access();
        let mut iter = CountingIterator::new(access).take(10000);
        let needle = iter.find(|x| x == "abcba");
        assert!(needle.is_none());
    }

    #[test]
    fn test_bcba_is_word() {
        let access = create_access();
        let mut iter = CountingIterator::new(access).take(100);
        let needle = iter.find(|x| x == "bcba");
        assert!(!needle.is_none());
    }

    #[test]
    fn test_a_is_first_word() {
        let access = create_access();
        let mut iter = CountingIterator::new(access);
        assert_eq!("a", iter.next().unwrap());
    }
}