# alphabet

A Rust crate providing utilities for working with alphabets. Documentation is available on [docs.rs](https://docs.rs/alphabet).

## Usage

Add this to your `Cargo.toml`:
```toml
[dependencies]
alphabet = "0.2"
```

## Getting Started

```rs
use alphabet::*;

alphabet!(BINARY = "01");
let mut words = BINARY.iter_words();
assert_eq!(words.next().unwrap(), "");
assert_eq!(words.next().unwrap(), "0");
assert_eq!(words.next().unwrap(), "1");
assert_eq!(words.next().unwrap(), "00");
assert_eq!(words.next().unwrap(), "01");
assert_eq!(words.next().unwrap(), "10");
```

## License

This crate is published under the terms of the MIT license. See the LICENSE file for details.